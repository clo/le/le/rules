# -*-makefile-*-
# $Id: template 4565 2006-02-10 14:23:10Z mkl $
#
# Copyright (C) 2006 by Erwin Rol
#          
# See CREDITS for details about who has contributed to this project.
#
# For further information about the PTXdist project and license conditions
# see the README file.
#

# FIXME, we only need the source tree, do we still need the package ?


#
# We provide this package
#
PACKAGES-$(PTXCONF_MESALIB) += mesalib

#
# Paths and names
#
MESALIB_VERSION	:= 7.4.2
MESALIB		:= MesaLib-$(MESALIB_VERSION)
MESALIB_SUFFIX	:= tar.bz2
MESALIB_URL	:= http://nchc.dl.sourceforge.net/sourceforge/mesa3d/$(MESALIB).$(MESALIB_SUFFIX)
MESALIB_SOURCE	:= $(SRCDIR)/$(MESALIB).$(MESALIB_SUFFIX)
MESALIB_DIR	:= $(BUILDDIR)/Mesa-$(MESALIB_VERSION)

# ----------------------------------------------------------------------------
# Get
# ----------------------------------------------------------------------------

$(MESALIB_SOURCE):
	@$(call targetinfo)
	@$(call get, MESALIB)

# ----------------------------------------------------------------------------
# Prepare
# ----------------------------------------------------------------------------

MESALIB_PATH  := PATH=$(CROSS_PATH)
MESALIB_ENV   := $(CROSS_ENV)

#
# autoconf
#
MESALIB_AUTOCONF := $(CROSS_AUTOCONF_USR)\

$(STATEDIR)/mesalib.prepare:
	@$(call targetinfo)
	@$(call clean, $(MESALIB_DIR)/config.cache)
	cd $(MESALIB_DIR) && \
	$(MESALIB_PATH) $(MESALIB_ENV)\
	./configure $(MESALIB_AUTOCONF)
	@$(call touch)

# ----------------------------------------------------------------------------
# Compile
# ----------------------------------------------------------------------------

$(STATEDIR)/mesalib.compile:
	@$(call targetinfo)
	cd $(MESALIB_DIR) && $(MESALIB_PATH) $(MAKE) $(PARALLELMFLAGS)
	@$(call touch)

# ----------------------------------------------------------------------------
# Install
# ----------------------------------------------------------------------------

$(STATEDIR)/mesalib.install:
	@$(call targetinfo)
	cp -a $(MESALIB_DIR)/include/*  $(SYSROOT)/usr/include/
	@$(call install, MESALIB)
	@$(call touch)

# ----------------------------------------------------------------------------
# Target-Install
# ----------------------------------------------------------------------------

$(STATEDIR)/mesalib.targetinstall:
	@$(call targetinfo)
	@$(call touch)

# ----------------------------------------------------------------------------
# Clean
# ----------------------------------------------------------------------------

mesalib_clean:
	rm -rf $(STATEDIR)/mesalib.*
	rm -rf $(PKGDIR)/mesalib_*
	rm -rf $(MESALIB_DIR)

# vim: syntax=make
