# -*-makefile-*-
# $Id: kernel.make 8150 2008-05-09 13:52:50Z mkl $
#
# Copyright (C) 2002-2008 by Pengutronix e.K., Hildesheim, Germany
#
# See CREDITS for details about who has contributed to this project.
#
# For further information about the PTXdist project and license conditions
# see the README file.
#

#
# We provide this package
#
PACKAGES-$(PTXCONF_KERNEL) += kernel

#
# handle special compilers
#
ifdef PTXCONF_KERNEL
    ifneq ($(PTXCONF_COMPILER_PREFIX),$(PTXCONF_COMPILER_PREFIX_KERNEL))
        ifeq ($(wildcard .ktoolchain/$(PTXCONF_COMPILER_PREFIX_KERNEL)gcc),)
            $(warning *** no .ktoolchain link found. Please create a link)
            $(warning *** .ktoolchain to the bin directory of your $(PTXCONF_COMPILER_PREFIX_KERNEL) toolchain)
            $(error )
        endif
    KERNEL_TOOLCHAIN_LINK := $(PTXDIST_WORKSPACE)/.ktoolchain/
    endif
endif

#
# Paths and names
#
KERNEL			:= linux-$(KERNEL_VERSION)
KERNEL_SUFFIX	:= tar.gz
KERNEL_TESTING	 = $(subst rc,testing/,$(findstring rc,$(KERNEL_VERSION)))


KERNEL_SRC_PATH := $(call remove_quotes, $(PTXCONF_KERNEL_SRC_PATH))

# use source controlled copy by overriding KERNEL_URL
# with user set path in ptxconfig
ifneq "$(KERNEL_SRC_PATH)" ""
KERNEL_URL       = file://$(KERNEL_SRC_PATH)
else
# use downloaded android kernel
KERNEL_FILENAME := "$(call remove_quotes, $(PTXCONF_KERNEL_FILENAME))"
KERNEL_URL 		+= "$(call remove_quotes,$(PTXCONF_KERNEL_URL))/$(KERNEL_FILENAME)"
endif	# PTXCONF_KERNEL_SRC_PATH

ifeq "$(KERNEL_SRC_PATH)" ""
KERNEL_SOURCE	:= $(SRCDIR)/$(KERNEL).$(KERNEL_SUFFIX)
endif
KERNEL_DIR		:= $(BUILDDIR)/$(KERNEL)
# XXX KERNEL_OBJDIR points to the same place where android kernel unrolls. 
# This is required because android kernel is built differently than how local 
# kernel is built. In case of android kernel, each target has it's own
# build-target area where the kernel source gets unrolled into a folder called
# "kernel".
KERNEL_OBJDIR   := $(BUILDDIR)/kernel
KERNEL_PKGDIR	:= $(PKGDIR)/$(KERNEL)

#
# Some configuration stuff for the different kernel image formats
#
ifdef PTXCONF_KERNEL_IMAGE_Z
KERNEL_IMAGE_PATH	:= $(KERNEL_OBJDIR)/arch/$(PTXCONF_KERNEL_ARCH_STRING)/boot/zImage
endif

ifdef PTXCONF_KERNEL_IMAGE_BZ
KERNEL_IMAGE_PATH	:= $(KERNEL_OBJDIR)/arch/$(PTXCONF_KERNEL_ARCH_STRING)/boot/bzImage
endif

ifdef PTXCONF_KERNEL_IMAGE_U
KERNEL_IMAGE_PATH	:= \
	$(KERNEL_OBJDIR)/uImage \
	$(KERNEL_OBJDIR)/arch/$(PTXCONF_KERNEL_ARCH_STRING)/boot/uImage \
	$(KERNEL_OBJDIR)/arch/$(PTXCONF_KERNEL_ARCH_STRING)/boot/images/uImage \
	$(KERNEL_OBJDIR)/arch/$(PTXCONF_KERNEL_ARCH_STRING)/boot/images/vmlinux.UBoot
endif

ifdef PTXCONF_KERNEL_IMAGE_VM
KERNEL_IMAGE_PATH	:= $(KERNEL_OBJDIR)/arch/$(PTXCONF_KERNEL_ARCH_STRING)/boot/vmImage
endif

ifdef PTXCONF_KERNEL_IMAGE_VMLINUX
KERNEL_IMAGE_PATH	:= $(KERNEL_OBJDIR)/vmlinux
endif

ifeq ($(PTXCONF_GCOV_SUPPORT),y)
GCOV_PATCH	:= $(KERNEL)-gcov
GCOV_PATCH_LIST := $(GCOV_PATCH).patch
GCOV_PATCH_LIST += $(GCOV_PATCH)-arm-eabi.patch
GCOV_PATCH_LIST += $(GCOV_PATCH)-arm-hack.patch
GCOV_PATCH_URL :=http://ltp.cvs.sourceforge.net/ltp/utils/analysis/gcov-kernel
GCOV_PATCH_SOURCE :=$(SRCDIR)/$(GCOV_PATCH).tgz
kernel_gcov_patch_get = 						\
    for GCOV_PATCH_FILE in $(GCOV_PATCH_LIST);				\
	do 								\
	$(WGET) -O $$GCOV_PATCH_FILE 					\
		$(GCOV_PATCH_URL)/$$GCOV_PATCH_FILE?revision=1.1 ;	\
	echo $$GCOV_PATCH_FILE>>series;					\
    done; 								\
    tar --remove-files -cvzf $(GCOV_PATCH_SOURCE)			\
		 series $(GCOV_PATCH_LIST)
endif

# ----------------------------------------------------------------------------
# Get
# ----------------------------------------------------------------------------

# The current git commit ID is cached here. Force a kernel download if
# it is not present or differs from PTXCONF_KERNEL_COMMIT_ID.
# This removes the need for everyone to manually delete prior downloads.
GIT_MERGE_ID := $(SRCDIR)/git-merge-id

$(STATEDIR)/kernel.get:
	@$(call targetinfo, $@)
ifeq ($(PTXCONF_GCOV_SUPPORT),y)
	$(if $(wildcard $(GCOV_PATCH_SOURCE)),,$(call kernel_gcov_patch_get))
endif
ifeq "$(KERNEL_SRC_PATH)" ""
	@echo "configured commit id: $(PTXCONF_KERNEL_COMMIT_ID)"
	@echo "downloaded commit id: $(shell cat $(GIT_MERGE_ID))"
# don't have an existing git id --force a download
	$(if $(wildcard $(GIT_MERGE_ID)),,$(call get, KERNEL))
# the git id differs from the config file --force a download
	$(if $(filter $(PTXCONF_KERNEL_COMMIT_ID),"$(shell cat $(GIT_MERGE_ID))"),,$(call get, KERNEL))
# not only does the URL dynamically change to include 'index.html' the tar.gz
# is saved using the full URL filename. rename it to the expected value.
	$(shell mv -f "$(SRCDIR)/index.html$(call remove_quotes,$(KERNEL_FILENAME))" $(KERNEL_SOURCE))
	@echo "$(PTXCONF_KERNEL_COMMIT_ID)" > $(GIT_MERGE_ID)
endif	# KERNEL_SRC_PATH
	@$(call touch, $@)

ifeq "$(KERNEL_SRC_PATH)" ""
$(KERNEL_SOURCE):
	@$(call targetinfo, $@)
	$(if $(filter $(PTXCONF_KERNEL_COMMIT_ID),"$(shell cat $(GIT_MERGE_ID))"),,$(call get, KERNEL))
	$(shell mv -f "$(SRCDIR)/index.html$(call remove_quotes,$(KERNEL_FILENAME))" $(KERNEL_SOURCE))
# the tarball wasn't previously downloaded... consider it current
	@echo "$(PTXCONF_KERNEL_COMMIT_ID)" > $(GIT_MERGE_ID)
endif	# KERNEL_SRC_PATH

# ----------------------------------------------------------------------------
# Extract
# ----------------------------------------------------------------------------

$(STATEDIR)/kernel.extract:
	@$(call targetinfo, $@)
	@$(call clean, $(KERNEL_DIR))
ifneq "$(KERNEL_SRC_PATH)" ""
# if a local path is provided, ptxdist doesn't create
# this directory (follow $$DEST in Rules.make extract routine) 
# before trying to create a symlink to the local KERNEL
# so we have to do it
	@[ -d $(BUILDDIR) ] || mkdir -p $(BUILDDIR)
endif
	@$(call extract, KERNEL)
ifeq ($(PTXCONF_GCOV_SUPPORT),y)
	@$(call extract, GCOV_PATCH, $(PTXDIST_WORKSPACE)/patches/$(GCOV_PATCH)/generic)
endif
ifeq "$(KERNEL_SRC_PATH)" ""
# downloaded kernel needs a symlink
# kernel.org unrolls the kernel to ./linux-2.6.27/
# prior to android release on 10/22, android.com untars kernel to ./kernel/
# afterwards git.source.android.com untars kernel to ./msm/
# ptxdist expects ./linux-2.6.27/
	@echo "using android kernel $(PTXCONF_KERNEL_COMMIT_ID)"
	if [ -d $(BUILDDIR)/msm ]; then \
		mv -f $(BUILDDIR)/msm $(BUILDDIR)/kernel; \
	fi
	cd $(BUILDDIR) && ln -sf kernel $(KERNEL)
	@$(call patchin, KERNEL)
ifeq ($(PTXCONF_GCOV_SUPPORT),y)
	@$(call patchin, GCOV_PATCH, $(BUILDDIR)/$(KERNEL))
endif
	rm -f $(BUILDDIR)/$(KERNEL)/GNUmakefile
else	# KERNEL_SRC_PATH
	@echo "using local kernel: $(KERNEL_SRC_PATH)"
	mkdir -p $(KERNEL_OBJDIR)
endif	# KERNEL_SRC_PATH
	@$(call touch, $@)

# ----------------------------------------------------------------------------
# Prepare
# ----------------------------------------------------------------------------

KERNEL_PATH	:= PATH=$(CROSS_PATH)
KERNEL_ENV 	:= KCONFIG_NOTIMESTAMP=1
KERNEL_MAKEVARS := \
	$(PARALLELMFLAGS) \
	HOSTCC=$(HOSTCC) \
	ARCH=$(PTXCONF_KERNEL_ARCH_STRING) \
	CROSS_COMPILE=$(KERNEL_TOOLCHAIN_LINK)$(PTXCONF_COMPILER_PREFIX_KERNEL) \
	\
	INSTALL_MOD_PATH=$(KERNEL_PKGDIR) \
	PTX_KERNEL_DIR=$(KERNEL_DIR)

ifneq "$(KERNEL_SRC_PATH)" ""
KERNEL_MAKEVARS +=O=$(KERNEL_OBJDIR)
endif

ifneq "$(KERNEL_SRC_PATH)" ""
KERNEL_CONFIG :=$(KERNEL_SRC_PATH)/arch/arm/configs/$(KERNEL_DEFCONFIG)
else
KERNEL_CONFIG :=$(BUILDDIR)/$(KERNEL)/arch/arm/configs/$(KERNEL_DEFCONFIG)
endif

# QCOM additional flags
KERNEL_MAKEVARS += TARGET=$(PTXCONF_PLATFORM)
KERNEL_MAKEVARS += TARGET_TYPE=$(TARGET_TYPE)
KERNEL_MAKEVARS += TARGET_PHYS_OFFSET=$(PTXCONF_BOOTWEDGE_PADDR)
# allow kernel Makefile to override VERSION
KERNEL_MAKEVARS += VERSION=2

ifdef PTXCONF_KERNEL_MODULES_INSTALL
KERNEL_MAKEVARS += \
	DEPMOD=$(PTXCONF_SYSROOT_CROSS)/sbin/$(PTXCONF_GNU_TARGET)-depmod
endif

KERNEL_IMAGE	:= $(PTXCONF_KERNEL_IMAGE)

$(KERNEL_CONFIG):

$(STATEDIR)/kernel.prepare: $(KERNEL_CONFIG) $(STATEDIR)/cross-module-init-tools.install
	@$(call targetinfo, $@)

	@if [ -f $(KERNEL_CONFIG) ]; then				\
		@echo "Using kernel config file: $(KERNEL_CONFIG)";	\
		install -m 644 $(KERNEL_CONFIG) $(KERNEL_OBJDIR)/.config; 	\
	else								\
		@echo "ERROR: No such kernel config: $(KERNEL_CONFIG)";	\
		exit 1;							\
	fi

ifdef PTXCONF_KLIBC
# tell the kernel where our spec file for initramfs is
	@sed -i -e 's,^CONFIG_INITRAMFS_SOURCE.*$$,CONFIG_INITRAMFS_SOURCE=\"$(KLIBC_CONTROL)\",g' \
		$(KERNEL_OBJDIR)/.config
endif

	cd $(KERNEL_DIR) && $(KERNEL_PATH) $(KERNEL_ENV) $(MAKE) \
		-f Makefile $(KERNEL_MAKEVARS) oldconfig

	cp -f $(KERNEL_OBJDIR)/.config $(KERNEL_CONFIG)

	@$(call touch, $@)

# ----------------------------------------------------------------------------
# Compile
# ----------------------------------------------------------------------------

$(STATEDIR)/kernel.compile:
	@$(call targetinfo, $@)
	cd $(KERNEL_DIR) && cd `pwd -P` && $(KERNEL_PATH) $(MAKE) \
		$(KERNEL_MAKEVARS) $(KERNEL_IMAGE) $(PTXCONF_KERNEL_MODULES_BUILD)
	@echo "Stripping symbols from vmlinux"
	cd $(KERNEL_OBJDIR) && $(COMPILER_PREFIX)strip --strip-all -o vmlinux.stripped vmlinux    
# create busybox .init_enable_core, target_type
	touch $(PTXDIST_WORKSPACE)/projectroot/.init_enable_core
	echo $(TARGET_TYPE) > $(PTXDIST_WORKSPACE)/projectroot/etc/target_type
	@$(call touch, $@)

# ----------------------------------------------------------------------------
# Install
# ----------------------------------------------------------------------------

$(STATEDIR)/kernel.install:
	@$(call targetinfo, $@)
	@$(call touch, $@)

# ----------------------------------------------------------------------------
# Target-Install
# ----------------------------------------------------------------------------

$(STATEDIR)/kernel.targetinstall:
	@$(call targetinfo, $@)

# we _always_ need the kernel in the image dir
	@for i in $(KERNEL_IMAGE_PATH); do				\
		if [ -f $$i ]; then					\
			install -m 644 $$i $(IMAGEDIR)/linuximage;	\
		fi;							\
	done

	@if test \! -e $(IMAGEDIR)/linuximage; then				\
		@echo "$(PTXCONF_KERNEL_IMAGE) not found, maybe bzImage on ARM";	\
		exit 1;								\
	fi

#
# install the ELF kernel image for debugging purpose
# e.g. oprofile
#
ifdef PTXCONF_KERNEL_VMLINUX
	$(call install_copy, kernel, 0, 0, 0644, $(KERNEL_OBJDIR)/vmlinux, /boot/vmlinux, n)
endif

ifdef PTXCONF_KERNEL_INSTALL
	@$(call install_init,  kernel)
	@$(call install_fixup, kernel, PACKAGE, kernel)
	@$(call install_fixup, kernel, PRIORITY,optional)
	@$(call install_fixup, kernel, VERSION,$(KERNEL_VERSION))
	@$(call install_fixup, kernel, SECTION,base)
	@$(call install_fixup, kernel, DEPENDS,)
	@$(call install_fixup, kernel, DESCRIPTION,missing)

	@for i in $(KERNEL_IMAGE_PATH); do 				\
		if [ -f $$i ]; then					\
			$(call install_copy, kernel, 0, 0, 0644, $$i, /boot/$(KERNEL_IMAGE), n); \
		fi;							\
	done

	@$(call install_finish, kernel)
endif

ifdef PTXCONF_KERNEL_MODULES_INSTALL
	if test -e $(KERNEL_PKGDIR); then \
		rm -rf $(KERNEL_PKGDIR); \
	fi
	cd $(KERNEL_DIR) && $(KERNEL_PATH) $(MAKE) \
		$(KERNEL_MAKEVARS) modules_install
endif

	@$(call touch, $@)


# ----------------------------------------------------------------------------
# Target-Install-post
# ----------------------------------------------------------------------------

$(STATEDIR)/kernel.targetinstall.post:
	@$(call targetinfo, $@)

ifdef PTXCONF_KERNEL_MODULES_INSTALL
	@$(call install_init,  kernel-modules)
	@$(call install_fixup, kernel-modules, PACKAGE,kernel-modules)
	@$(call install_fixup, kernel-modules, PRIORITY,optional)
	@$(call install_fixup, kernel-modules, VERSION,$(KERNEL_VERSION))
	@$(call install_fixup, kernel-modules, SECTION,base)
	@$(call install_fixup, kernel-modules, AUTHOR,"Robert Schwebel <r.schwebel\@pengutronix.de>")
	@$(call install_fixup, kernel-modules, DEPENDS,)
	@$(call install_fixup, kernel-modules, DESCRIPTION,missing)

	@cd $(KERNEL_PKGDIR) &&					\
		for file in `find . -type f | sed -e "s/\.\//\//g"`; do	\
			$(call install_copy, kernel-modules, 0, 0, 0644, $(KERNEL_PKGDIR)/$${file}, $${file}, n); \
		done

# XXX move to generic rootfs handler
# install .init_enable core, target_type
	$(call install_copy, kernel-modules, 0, 0, 0644, $(PTXDIST_WORKSPACE)/projectroot/.init_enable_core, /.init_enable_core, n)
	$(call install_copy, kernel-modules, 0, 0, 0644, $(PTXDIST_WORKSPACE)/projectroot/etc/target_type, /etc/target_type, n)

ifeq ($(PTXCONF_GCOV_SUPPORT),y)
# install gcov scripts into /home
	@$(call install_copy, kernel-modules, 0, 0, 0755, $(PTXDIST_WORKSPACE)/projectroot/home/copy_counters.sh, /home/copy_counters.sh)
	@$(call install_copy, kernel-modules, 0, 0, 0755, $(PTXDIST_WORKSPACE)/projectroot/home/zero_counters.sh, /home/zero_counters.sh)
endif
	@$(call install_finish, kernel-modules)
endif

	@$(call touch, $@)

# ----------------------------------------------------------------------------
# Clean
# ----------------------------------------------------------------------------

kernel_clean:
	rm -rf $(STATEDIR)/kernel.*
	rm -rf $(PKGDIR)/kernel_*
	rm -rf $(KERNEL_DIR)
ifeq ($(PTXCONF_GCOV_SUPPORT),y)
	rm -rf $(PTXDIST_WORKSPACE)/patches/$(GCOV_PATCH)
endif

# ----------------------------------------------------------------------------
# oldconfig / menuconfig
# ----------------------------------------------------------------------------

kernel_oldconfig kernel_menuconfig: $(STATEDIR)/kernel.extract
	@if test -e $(KERNEL_CONFIG); then \
		cp -f $(KERNEL_CONFIG) $(KERNEL_OBJDIR)/.config; \
	fi
	@cd $(KERNEL_DIR) && \
		$(KERNEL_PATH) $(KERNEL_ENV) $(MAKE) $(KERNEL_MAKEVARS) $(subst kernel_,,$@)
	@if cmp -s $(KERNEL_OBJDIR)/.config $(KERNEL_CONFIG); then \
		@echo "kernel configuration unchanged"; \
	else \
		cp -f $(KERNEL_OBJDIR)/.config $(KERNEL_CONFIG); \
	fi


# vim: syntax=make
