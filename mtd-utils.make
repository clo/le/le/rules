# -*-makefile-*-
# $Id: mtd-utils.make 6728 2007-01-04 11:53:52Z rsc $
#
# Copyright (C) 2003-2006 by Pengutronix e.K., Hildesheim, Germany
#
# See CREDITS for details about who has contributed to this project.
#
# For further information about the PTXdist project and license conditions
# see the README file.
#

#
# We provide this package
#
PACKAGES-$(PTXCONF_MTD_UTILS) += mtd-utils

#
# Paths and names
#
MTD_UTILS_VERSION	:= 20080508
MTD_UTILS		:= mtd-utils
MTD_UTILS_SUFFIX	:= orig.tar.gz
MTD_UTILS_SRC_PATH      := $(call remove_quotes, $(PTXCONF_MTD_UTILS_SRC_PATH))
ifneq "$(MTD_UTILS_SRC_PATH)" ""
MTD_UTILS_URL           := file://$(MTD_UTILS_SRC_PATH)
else
# use downloaded mtd-utils
MTD_UTILS_FILENAME      := "$(call remove_quotes, $(PTXCONF_MTD_UTILS_FILENAME))"
MTD_UTILS_URL 		:= "$(call remove_quotes, $(PTXCONF_MTD_UTILS_URL))/$(MTD_UTILS_FILENAME)"
MTD_UTILS_SOURCE	:= $(SRCDIR)/$(MTD_UTILS).$(MTD_UTILS_SUFFIX)
endif
MTD_UTILS_DIR		:= $(BUILDDIR)/$(MTD_UTILS)

ifeq "$(MTD_UTILS_SRC_PATH)" ""
# XXX this is just wrong
# $2 defaults to the src directory... do not set
fix_broken_mtd_get = \
    URL="$($(strip $(1))_URL)";     \
    SRC="$(strip $(2))";            \
    SRC=$${SRC:-$(SRCDIR)};         \
    [ -d $$SRC ] || mkdir -p $$SRC; \
        $(WGET) -O $(MTD_UTILS_SOURCE) -P $$SRC $$URL;     \
        [ $$? -eq 0 ] || {          \
            echo;                   \
            echo "Could not get packet via http!";  \
            echo "URL: $$URL";      \
            echo;                   \
            exit -1;                \
            };
endif
# ----------------------------------------------------------------------------
# Get
# ----------------------------------------------------------------------------

mtd-utils_get: $(STATEDIR)/mtd-utils.get

$(STATEDIR)/mtd-utils.get: $(mtd-utils_get_deps_default)
	@$(call targetinfo, $@)
	@$(call touch, $@)
ifeq "$(MTD_UTILS_SRC_PATH)" ""
# ptxdist expects mtd-utils-20080508.orig.tar.gz
	$(shell mv -f "$(SRCDIR)/index.html$(call remove_quotes,$(MTD_UTILS_FILENAME))" $(MTD_UTILS_SOURCE))
endif

$(MTD_UTILS_SOURCE):
	@$(call targetinfo, $@)
ifeq "$(MTD_UTILS_SRC_PATH)" ""
	@$(call get, MTD_UTILS)
endif

# ----------------------------------------------------------------------------
# Extract
# ----------------------------------------------------------------------------

mtd-utils_extract: $(STATEDIR)/mtd-utils.extract

$(STATEDIR)/mtd-utils.extract: $(mtd-utils_extract_deps_default)
	@$(call targetinfo, $@)
	@$(call clean, $(MTD_UTILS_DIR))
ifneq "$(MTD_UTILS_SRC_PATH)" ""
# if a local path is provided, ptxdist doesn't create
# this directory (follow $$DEST in Rules.make extract routine) 
# before trying to create a symlink to the local MTD_UTILS
# so we have to do it
	@[ -d $(BUILDDIR) ] || mkdir -p $(BUILDDIR)
endif
	@$(call extract, MTD_UTILS)
ifeq "$(MTD_UTILS_SRC_PATH)" ""
	@$(call patchin, MTD_UTILS)
else 
	@echo "using local mtd utils: $(MTD_UTILS_SRC_PATH)"
endif
	@$(call touch, $@)

# ----------------------------------------------------------------------------
# Prepare
# ----------------------------------------------------------------------------

mtd-utils_prepare: $(STATEDIR)/mtd-utils.prepare

MTD_UTILS_PATH		:= PATH=$(CROSS_PATH)
MTD_UTILS_ENV 		:= $(CROSS_ENV)
MTD_UTILS_MAKEVARS	:= \
	CROSS="$(COMPILER_PREFIX)" \
	CPPFLAGS="$(CROSS_CPPFLAGS)" \
	LDFLAGS="$(CROSS_LDFLAGS)"

$(STATEDIR)/mtd-utils.prepare: $(mtd-utils_prepare_deps_default)
	@$(call targetinfo, $@)
	@$(call touch, $@)

# ----------------------------------------------------------------------------
# Compile
# ----------------------------------------------------------------------------

mtd-utils_compile: $(STATEDIR)/mtd-utils.compile

$(STATEDIR)/mtd-utils.compile: $(mtd-utils_compile_deps_default)
	@$(call targetinfo, $@)
	cd $(MTD_UTILS_DIR) && $(MTD_UTILS_PATH) $(MAKE) $(PARALLELMFLAGS) $(MTD_UTILS_MAKEVARS)
	@$(call touch, $@)

# ----------------------------------------------------------------------------
# Install
# ----------------------------------------------------------------------------

mtd-utils_install: $(STATEDIR)/mtd-utils.install

$(STATEDIR)/mtd-utils.install: $(mtd-utils_install_deps_default)
	@$(call targetinfo, $@)
	@$(call touch, $@)

# ----------------------------------------------------------------------------
# Target-Install
# ----------------------------------------------------------------------------

mtd-utils_targetinstall: $(STATEDIR)/mtd-utils.targetinstall

$(STATEDIR)/mtd-utils.targetinstall: $(mtd-utils_targetinstall_deps_default)
	@$(call targetinfo, $@)

	@$(call install_init, mtd-utils)
	@$(call install_fixup, mtd-utils,PACKAGE,mtd-utils)
	@$(call install_fixup, mtd-utils,PRIORITY,optional)
	@$(call install_fixup, mtd-utils,VERSION,$(MTD_UTILS_VERSION))
	@$(call install_fixup, mtd-utils,SECTION,base)
	@$(call install_fixup, mtd-utils,DEPENDS,)
	@$(call install_fixup, mtd-utils,DESCRIPTION,missing)
	#ptxdist expects these executables in the mtd_utils dir but they're located in 
	# $(mtd_utils_dir)/arm-none-linux-gnueabi

	# short term hack, copy into mtd_utils_dir
	cp $(MTD_UTILS_DIR)/arm-none-linux-gnueabi/* $(MTD_UTILS_DIR)/

ifdef PTXCONF_MTD_UTILS_FLASH_ERASE
	@$(call install_copy, mtd-utils, 0, 0, 0755, $(MTD_UTILS_DIR)/flash_erase, /sbin/flash_erase)
endif
ifdef PTXCONF_MTD_UTILS_FLASH_ERASEALL
	@$(call install_copy, mtd-utils, 0, 0, 0755, $(MTD_UTILS_DIR)/flash_eraseall, /sbin/flash_eraseall)
endif
ifdef PTXCONF_MTD_UTILS_FLASH_INFO
	@$(call install_copy, mtd-utils, 0, 0, 0755, $(MTD_UTILS_DIR)/flash_info, /sbin/flash_info)
endif
ifdef PTXCONF_MTD_UTILS_FLASH_LOCK
	@$(call install_copy, mtd-utils, 0, 0, 0755, $(MTD_UTILS_DIR)/flash_lock, /sbin/flash_lock)
endif
ifdef PTXCONF_MTD_UTILS_FLASH_UNLOCK
	@$(call install_copy, mtd-utils, 0, 0, 0755, $(MTD_UTILS_DIR)/flash_unlock, /sbin/flash_unlock)
endif
ifdef PTXCONF_MTD_UTILS_FLASHCP
	@$(call install_copy, mtd-utils, 0, 0, 0755, $(MTD_UTILS_DIR)/flashcp, /sbin/flashcp)
endif
ifdef PTXCONF_MTD_UTILS_FTL_CHECK
	@$(call install_copy, mtd-utils, 0, 0, 0755, $(MTD_UTILS_DIR)/ftl_check, /sbin/ftl_check)
endif
ifdef PTXCONF_MTD_UTILS_FTL_FORMAT
	@$(call install_copy, mtd-utils, 0, 0, 0755, $(MTD_UTILS_DIR)/ftl_format, /sbin/ftl_format)
endif
ifdef PTXCONF_MTD_UTILS_JFFS2_DUMP
	@$(call install_copy, mtd-utils, 0, 0, 0755, $(MTD_UTILS_DIR)/jffs2dump, /sbin/jffs2dump)
endif
#ifdef PTXCONF_MTD_UTILS_JFFS2READER
#	@$(call install_copy, mtd-utils, 0, 0, 0755, $(MTD_UTILS_DIR)/jffs2reader, /sbin/jffs2reader)
#endif
ifdef PTXCONF_MTD_UTILS_MTDDEBUG
	@$(call install_copy, mtd-utils, 0, 0, 0755, $(MTD_UTILS_DIR)/mtd_debug, /sbin/mtd_debug)
endif
ifdef PTXCONF_MTD_UTILS_NANDDUMP
	@$(call install_copy, mtd-utils, 0, 0, 0755, $(MTD_UTILS_DIR)/nanddump, /sbin/nanddump)
endif
ifdef PTXCONF_MTD_UTILS_NANDWRITE
	@$(call install_copy, mtd-utils, 0, 0, 0755, $(MTD_UTILS_DIR)/nandwrite, /sbin/nandwrite)
endif
ifdef PTXCONF_MTD_UTILS_NFTL_FORMAT
	@$(call install_copy, mtd-utils, 0, 0, 0755, $(MTD_UTILS_DIR)/nftl_format, /sbin/nftl_format)
endif
ifdef PTXCONF_MTD_UTILS_NFTLDUMP
	@$(call install_copy, mtd-utils, 0, 0, 0755, $(MTD_UTILS_DIR)/nftldump, /sbin/nftldump)
endif
ifdef PTXCONF_MTD_UTILS_MKJFFS
	@$(call install_copy, mtd-utils, 0, 0, 0755, $(MTD_UTILS_DIR)/mkfs.jffs, /sbin/mkfs.jffs)
endif
ifdef PTXCONF_MTD_UTILS_MKJFFS2
	@$(call install_copy, mtd-utils, 0, 0, 0755, $(MTD_UTILS_DIR)/mkfs.jffs2, /sbin/mkfs.jffs2)
endif
ifdef PTXCONF_MTD_UTILS_NANDTEST
	@$(call install_copy, mtd-utils, 0, 0, 0755, $(MTD_UTILS_DIR)/nandtest, /sbin/nandtest)
endif
ifdef PTXCONF_MTD_UTILS_DOCFDISK
	@$(call install_copy, mtd-utils, 0, 0, 0755, $(MTD_UTILS_DIR)/docfdisk, /sbin/docfdisk)
endif
ifdef PTXCONF_MTD_UTILS_DOC_LOADBIOS
	@$(call install_copy, mtd-utils, 0, 0, 0755, $(MTD_UTILS_DIR)/doc_loadbios, /sbin/doc_loadbios)
endif
ifdef PTXCONF_MTD_UTILS_FLASH_OTP_DUMP
	@$(call install_copy, mtd-utils, 0, 0, 0755, $(MTD_UTILS_DIR)/flash_otp_dump, /sbin/flash_otp_dump)
endif
ifdef PTXCONF_MTD_UTILS_FLASH_OTP_INFO
	@$(call install_copy, mtd-utils, 0, 0, 0755, $(MTD_UTILS_DIR)/flash_otp_info, /sbin/flash_otp_info)
endif
ifdef PTXCONF_MTD_UTILS_RFDDUMP
	@$(call install_copy, mtd-utils, 0, 0, 0755, $(MTD_UTILS_DIR)/rfddump, /sbin/rfddump)
endif
ifdef PTXCONF_MTD_UTILS_RFDFORMAT
	@$(call install_copy, mtd-utils, 0, 0, 0755, $(MTD_UTILS_DIR)/rfdformat, /sbin/rfdformat)
endif
ifdef PTXCONF_MTD_UTILS_SERVE_IMAGE
	@$(call install_copy, mtd-utils, 0, 0, 0755, $(MTD_UTILS_DIR)/serve_image, /sbin/serve_image)
endif
ifdef PTXCONF_MTD_UTILS_RECV_IMAGE
	@$(call install_copy, mtd-utils, 0, 0, 0755, $(MTD_UTILS_DIR)/recv_image, /sbin/recv_image)
endif
ifdef PTXCONF_MTD_UTILS_SUMTOOL
	@$(call install_copy, mtd-utils, 0, 0, 0755, $(MTD_UTILS_DIR)/sumtool, /sbin/sumtool)
endif

	@$(call install_finish, mtd-utils)

	@$(call touch, $@)

# ----------------------------------------------------------------------------
# Clean
# ----------------------------------------------------------------------------

mtd-utils_clean:
	rm -rf $(STATEDIR)/mtd-utils.*
	rm -rf $(PKGDIR)/mtd-utils_*
	rm -rf $(MTD_UTILS_DIR)

# vim: syntax=make
