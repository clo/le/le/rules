################################################################################
################################################################################

#
# We provide this package
#
PACKAGES-$(PTXCONF_UTIL_MACROS) += util-macros

#
# Paths and names
#
UTIL_MACROS_VERSION	:= 1.2.1
UTIL_MACROS		:= util-macros-$(UTIL_MACROS_VERSION)
UTIL_MACROS_SUFFIX	:= tar.gz
UTIL_MACROS_URL		:= http://ftp.x.org/pub/individual/util/$(UTIL_MACROS).$(UTIL_MACROS_SUFFIX)
UTIL_MACROS_SOURCE	:= $(SRCDIR)/$(UTIL_MACROS).$(UTIL_MACROS_SUFFIX)
UTIL_MACROS_DIR		:= $(BUILDDIR)/$(UTIL_MACROS)

# ----------------------------------------------------------------------------
# Get
# ----------------------------------------------------------------------------

$(UTIL_MACROS_SOURCE):
	@$(call targetinfo)
	@$(call get, UTIL_MACROS)

# ----------------------------------------------------------------------------
# Extract
# ----------------------------------------------------------------------------

$(STATEDIR)/util-macros.extract:
	@$(call targetinfo)
	@$(call clean, $(UTIL_MACROS_DIR))
	@$(call extract, UTIL_MACROS)
	@$(call patchin, UTIL_MACROS)
	@$(call touch)

# ----------------------------------------------------------------------------
# Prepare
# ----------------------------------------------------------------------------

UTIL_MACROS_PATH	:= PATH=$(CROSS_PATH)
UTIL_MACROS_ENV 	:= $(CROSS_ENV)

#
# autoconf
#
UTIL_MACROS_AUTOCONF := $(CROSS_AUTOCONF_USR)

$(STATEDIR)/util-macros.prepare:
	@$(call targetinfo)
	@$(call clean, $(UTIL_MACROS_DIR)/config.cache)
	cd $(UTIL_MACROS_DIR) && \
		$(UTIL_MACROS_PATH) $(UTIL_MACROS_ENV) \
		./configure $(UTIL_MACROS_AUTOCONF)
	@$(call touch)

# ----------------------------------------------------------------------------
# Compile
# ----------------------------------------------------------------------------

$(STATEDIR)/util-macros.compile:
	@$(call targetinfo)
	cd $(UTIL_MACROS_DIR) && $(UTIL_MACROS_PATH) $(MAKE) $(PARALLELMFLAGS)
	@$(call touch)

# ----------------------------------------------------------------------------
# Install
# ----------------------------------------------------------------------------

$(STATEDIR)/util-macros.install:
	@$(call targetinfo)
	@$(call install, UTIL_MACROS)
	@$(call touch)

# ----------------------------------------------------------------------------
# Target-Install
# ----------------------------------------------------------------------------

$(STATEDIR)/util-macros.targetinstall:
	@$(call targetinfo)

	@$(call install_init, util-macros)
	@$(call install_fixup, util-macros,PACKAGE,util-macros)
	@$(call install_fixup, util-macros,PRIORITY,optional)
	@$(call install_fixup, util-macros,VERSION,$(UTIL_MACROS_VERSION))
	@$(call install_fixup, util-macros,SECTION,base)
	@$(call install_fixup, util-macros,AUTHOR,"")
	@$(call install_fixup, util-macros,DEPENDS,)
	@$(call install_fixup, util-macros,DESCRIPTION,missing)

#	@$(call install_copy, util-macros, 0, 0, 0755, $(UTIL_MACROS_DIR)/foobar, /dev/null)

	@$(call install_finish, util-macros)

	@$(call touch)

# ----------------------------------------------------------------------------
# Clean
# ----------------------------------------------------------------------------

util-macros_clean:
	rm -rf $(STATEDIR)/util-macros.*
	rm -rf $(PKGDIR)/util-macros_*
	rm -rf $(UTIL_MACROS_DIR)

# vim: syntax=make
