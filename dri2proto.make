################################################################################
################################################################################

#
# We provide this package
#
PACKAGES-$(PTXCONF_DRI2PROTO) += dri2proto

#
# Paths and names
#
DRI2PROTO_VERSION	:= 2.1
DRI2PROTO		:= dri2proto-$(DRI2PROTO_VERSION)
DRI2PROTO_SUFFIX	:= tar.gz
DRI2PROTO_URL		:= http://ftp.x.org/pub/individual/proto/$(DRI2PROTO).$(DRI2PROTO_SUFFIX)
DRI2PROTO_SOURCE	:= $(SRCDIR)/$(DRI2PROTO).$(DRI2PROTO_SUFFIX)
DRI2PROTO_DIR		:= $(BUILDDIR)/$(DRI2PROTO)

# ----------------------------------------------------------------------------
# Get
# ----------------------------------------------------------------------------

$(DRI2PROTO_SOURCE):
	@$(call targetinfo)
	@$(call get, DRI2PROTO)

# ----------------------------------------------------------------------------
# Extract
# ----------------------------------------------------------------------------

$(STATEDIR)/dri2proto.extract:
	@$(call targetinfo)
	@$(call clean, $(DRI2PROTO_DIR))
	@$(call extract, DRI2PROTO)
	@$(call patchin, DRI2PROTO)
	@$(call touch)

# ----------------------------------------------------------------------------
# Prepare
# ----------------------------------------------------------------------------

DRI2PROTO_PATH	:= PATH=$(CROSS_PATH)
DRI2PROTO_ENV 	:= $(CROSS_ENV)

#
# autoconf
#
DRI2PROTO_AUTOCONF := $(CROSS_AUTOCONF_USR)

$(STATEDIR)/dri2proto.prepare:
	@$(call targetinfo)
	@$(call clean, $(DRI2PROTO_DIR)/config.cache)
	cd $(DRI2PROTO_DIR) && \
		$(DRI2PROTO_PATH) $(DRI2PROTO_ENV) \
		./configure $(DRI2PROTO_AUTOCONF)
	@$(call touch)

# ----------------------------------------------------------------------------
# Compile
# ----------------------------------------------------------------------------

$(STATEDIR)/dri2proto.compile:
	@$(call targetinfo)
	cd $(DRI2PROTO_DIR) && $(DRI2PROTO_PATH) $(MAKE) $(PARALLELMFLAGS)
	@$(call touch)

# ----------------------------------------------------------------------------
# Install
# ----------------------------------------------------------------------------

$(STATEDIR)/dri2proto.install:
	@$(call targetinfo)
	@$(call install, DRI2PROTO)
	@$(call touch)

# ----------------------------------------------------------------------------
# Target-Install
# ----------------------------------------------------------------------------

$(STATEDIR)/dri2proto.targetinstall:
	@$(call targetinfo)

	@$(call install_init, dri2proto)
	@$(call install_fixup, dri2proto,PACKAGE,dri2proto)
	@$(call install_fixup, dri2proto,PRIORITY,optional)
	@$(call install_fixup, dri2proto,VERSION,$(DRI2PROTO_VERSION))
	@$(call install_fixup, dri2proto,SECTION,base)
	@$(call install_fixup, dri2proto,AUTHOR,"")
	@$(call install_fixup, dri2proto,DEPENDS,)
	@$(call install_fixup, dri2proto,DESCRIPTION,missing)

#	@$(call install_copy, dri2proto, 0, 0, 0755, $(DRI2PROTO_DIR)/foobar, /dev/null)

	@$(call install_finish, dri2proto)

	@$(call touch)

# ----------------------------------------------------------------------------
# Clean
# ----------------------------------------------------------------------------

dri2proto_clean:
	rm -rf $(STATEDIR)/dri2proto.*
	rm -rf $(PKGDIR)/dri2proto_*
	rm -rf $(DRI2PROTO_DIR)

# vim: syntax=make
