################################################################################
################################################################################

#
# We provide this package
#
HOST_PACKAGES-$(PTXCONF_HOST_YAFFS2) += host-yaffs2

#
# Paths and names
#
HOST_YAFFS2_VERSION	:= 2007-12-04
HOST_YAFFS2		:= yaffs2-$(HOST_YAFFS2_VERSION)
HOST_YAFFS2_SUFFIX	:= tar.gz
HOST_YAFFS2_URL		:= :pserver:anonymous@cvs.aleph1.co.uk:/home/aleph1/cvs/
HOST_YAFFS2_SOURCE	:= $(SRCDIR)/$(HOST_YAFFS2).$(HOST_YAFFS2_SUFFIX)
HOST_YAFFS2_DIR		:= $(HOST_BUILDDIR)/$(HOST_YAFFS2)

# XXX this is just wrong
get_yaffs2_cvs = \
    cvs $(QUIET_FLAG) -d $(HOST_YAFFS2_URL) checkout -D $(HOST_YAFFS2_VERSION) yaffs2; \
    [ $$? -eq 0 ] || {          \
        echo;                   \
        echo "Could not get packet via cvs!";  \
        echo "Command: cvs $(QUIET_FLAG) -d $(HOST_YAFFS2_URL) checkout -D $(HOST_YAFFS2_VERSION) yaffs2";      \
        echo;                   \
        exit -1;                \
        };

tar_and_remove =	\
    pwd;	\
    tar cvzf $(HOST_YAFFS2_SOURCE) yaffs2;	\
    [ $$? -eq 0 ] || {	\
	echo;	\
	echo "could not tar!";	\
	echo;	\
	exit -1;	\
    };	\
    rm -rf yaffs2;

# ----------------------------------------------------------------------------
# Get
# ----------------------------------------------------------------------------

host-yaffs2_get: $(STATEDIR)/host-yaffs2.get

$(STATEDIR)/host-yaffs2.get: $(host-yaffs2_get_deps_default)
	@$(call targetinfo, $@)
	@$(call touch, $@)

#set QUIET_FLAG to -Q for quiet mode
QUIET_FLAG := -Q

$(HOST_YAFFS2_SOURCE):
	@$(call targetinfo, $@)
	@$(call get_yaffs2_cvs)
	@$(call tar_and_remove)

# ----------------------------------------------------------------------------
# Extract
# ----------------------------------------------------------------------------

host-yaffs2_extract: $(STATEDIR)/host-yaffs2.extract

$(STATEDIR)/host-yaffs2.extract: $(host-yaffs2_extract_deps_default)
	@$(call targetinfo, $@)
	@$(call clean, $(HOST_YAFFS2_DIR))
	$(call extract, HOST_YAFFS2, $(HOST_BUILDDIR))
#if downloaded from cvs, create a symlink because
# ptxdist expects yaffs2-2007-12-04
	cd $(HOST_BUILDDIR) && ln -sf yaffs2 $(HOST_YAFFS2)
#endif
	@$(call patchin, HOST_YAFFS2, $(HOST_YAFFS2_DIR))
	@$(call touch, $@)

# ----------------------------------------------------------------------------
# Prepare
# ----------------------------------------------------------------------------

host-yaffs2_prepare: $(STATEDIR)/host-yaffs2.prepare

HOST_YAFFS2_PATH	:= PATH=$(HOST_PATH)
HOST_YAFFS2_ENV 	:= $(HOST_ENV)

HOST_YAFFS2_MAKEVARS += MTDUTILSDIR=$(MTD_UTILS_DIR)
HOST_YAFFS2_MAKEVARS += MKYAFFSOBJS=mkyaffs2.o

$(STATEDIR)/host-yaffs2.prepare: $(host-yaffs2_prepare_deps_default)
	@$(call targetinfo, $@)
	@$(call touch, $@)

# ----------------------------------------------------------------------------
# Compile
# ----------------------------------------------------------------------------

$(STATEDIR)/host-yaffs2.compile:
	@$(call targetinfo, $@)
	cd $(HOST_YAFFS2_DIR)/utils && $(HOST_YAFFS2_ENV) $(HOST_YAFFS2_PATH) $(MAKE) $(HOST_YAFFS2_MAKEVARS) $(PARALLELMFLAGS)
	@$(call touch, $@)

# ----------------------------------------------------------------------------
# Install
# ----------------------------------------------------------------------------

host-yaffs2_install: $(STATEDIR)/host-yaffs2.install

$(STATEDIR)/host-yaffs2.install: $(host-yaffs2_install_deps_default)
	@$(call targetinfo, $@)
	@-cp -f $(HOST_YAFFS2_DIR)/utils/mkyaffs2image $(PTXCONF_SYSROOT_HOST)/bin/mkyaffs2image
	@$(call touch, $@)

# ----------------------------------------------------------------------------
# Clean
# ----------------------------------------------------------------------------

host-yaffs2_clean:
	rm -rf $(STATEDIR)/host-yaffs2.*
	rm -rf $(HOST_YAFFS2_DIR)

# vim: syntax=make
