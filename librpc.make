# -*-makefile-*-
# For further information about the PTXdist project and license conditions
# see the README file.
#

#
# We provide this package
#
PACKAGES-$(PTXCONF_LIBRPC) += librpc

#
# Paths and names
#
LIBRPC_VERSION	 := 1.0.0
LIBRPC		 := librpc-$(LIBRPC_VERSION)
LIBRPC_SUFFIX	 := tar.gz
LIBRPC_DIR	 := $(BUILDDIR)/$(LIBRPC)
LIBRPC_SRC_PATH  := $(call remove_quotes, $(PTXCONF_LIBRPC_SRC_PATH))
ifneq "$(LIBRPC_SRC_PATH)" ""
LIBRPC_URL       := file://$(LIBRPC_SRC_PATH)
LIBRPC_SRCDIR    := $(PTXDIST_WORKSPACE)/../opensource/librpc/librpc
else
# use downloaded librpc
LIBRPC_FILENAME  := "$(call remove_quotes, $(PTXCONF_LIBRPC_FILENAME))"
LIBRPC_URL	 := "$(call remove_quotes, $(PTXCONF_LIBRPC_URL))/$(LIBRPC_FILENAME)"
LIBRPC_SOURCE	 := $(SRCDIR)/$(LIBRPC).$(LIBRPC_SUFFIX)
LIBRPC_SRCDIR    := $(LIBRPC_DIR)/msm7k/librpc
endif

# ----------------------------------------------------------------------------
# Get
# ----------------------------------------------------------------------------

librpc_get: $(STATEDIR)/librpc.get

$(STATEDIR)/librpc.get: $(librpc_get_deps_default)
	@$(call targetinfo, $@)
	@$(call touch, $@)
ifeq "$(LIBRPC_SRC_PATH)" ""
	$(shell mv -f "$(SRCDIR)/index.html$(call remove_quotes,$(LIBRPC_FILENAME))" $(LIBRPC_SOURCE))
endif

$(LIBRPC_SOURCE):
	@$(call targetinfo, $@)
ifeq "$(LIBRPC_SRC_PATH)" ""
	@$(call get, LIBRPC)
endif

# ----------------------------------------------------------------------------
# Extract
# ----------------------------------------------------------------------------

$(STATEDIR)/librpc.extract:
	@$(call targetinfo, $@)
# create a platform specific package directory for intermediate files.
# the default behavior is to create a symlink to the source directory.
ifeq "$(LIBRPC_SRC_PATH)" ""
	mkdir -p $(LIBRPC_DIR)
endif
	@$(call extract, LIBRPC)
ifeq "$(LIBRPC_SRC_PATH)" ""
# git.source.android.com untars librpc to msm7k
	@echo "using librpc $(PTXCONF_LIBRPC_COMMIT_ID)"
	if [ -d $(BUILDDIR)/msm7k ]; then \
		mv -f $(BUILDDIR)/msm7k $(BUILDDIR)/$(LIBRPC); \
	fi
else 
	@echo "using local librpc: $(LIBRPC_SRC_PATH)"
endif
	@$(call touch, $@)

# ----------------------------------------------------------------------------
# Prepare
# ----------------------------------------------------------------------------

LIBRPC_LIBVER   := $(LIBRPC_VERSION)
LIBRPC_LIBMAJOR := $(basename $(basename $(LIBRPC_LIBVER)))
LIBRPC_PATH := PATH=$(CROSS_PATH)
LIBRPC_ENV  := $(CROSS_ENV)
# Build in the platform-XXXX/build-<host|target> directory
LIBRPC_MAKEVARS += -C $(LIBRPC_DIR)
# Invoke the Makefile which resides with the source
LIBRPC_MAKEVARS += -f $(LIBRPC_SRCDIR)/librpc.mk
# Location of source directory used by vpath
LIBRPC_MAKEVARS += OBJDIR=$(LIBRPC_DIR)
LIBRPC_MAKEVARS += SRCDIR=$(LIBRPC_SRCDIR)

LIBRPC_MAKEVARS += LIBVER=$(LIBRPC_LIBVER)
LIBRPC_MAKEVARS += LIBMAJOR=$(LIBRPC_LIBMAJOR)
LIBRPC_MAKEVARS += $(PARALLELMFLAGS)

ifeq "$(PTXCONF_LIBRPC_TEST)" "y"
LIBRPC_MAKEVARS += BUILD_TEST=y
endif	# PTXCONF_LIBRPC_TEST

$(STATEDIR)/librpc.prepare:
	@$(call targetinfo, $@)
	@$(call touch, $@)

# ----------------------------------------------------------------------------
# Compile
# ----------------------------------------------------------------------------

$(STATEDIR)/librpc.compile:
	@$(call targetinfo, $@)
	$(LIBRPC_ENV) $(LIBRPC_PATH) $(MAKE) $(LIBRPC_MAKEVARS)
	@$(call touch, $@)

# ----------------------------------------------------------------------------
# Install
# ----------------------------------------------------------------------------

$(STATEDIR)/librpc.install:
	@$(call targetinfo, $@)
# install to common include and link paths
	@cp -f $(LIBRPC_SRCDIR)/*.h $(PTXCONF_SYSROOT_TARGET)/include
	@cp -f $(LIBRPC_DIR)/liblibrpc.so.$(LIBRPC_LIBVER) $(PTXCONF_SYSROOT_TARGET)/lib/liblibrpc.so.$(LIBRPC_LIBVER)
	@ln -sf $(PTXCONF_SYSROOT_TARGET)/lib/liblibrpc.so.$(LIBRPC_LIBVER) $(PTXCONF_SYSROOT_TARGET)/lib/liblibrpc.so.$(LIBRPC_LIBMAJOR)
	@ln -sf $(PTXCONF_SYSROOT_TARGET)/lib/liblibrpc.so.$(LIBRPC_LIBMAJOR) $(PTXCONF_SYSROOT_TARGET)/lib/liblibrpc.so
	@mkdir -p $(PTXCONF_SYSROOT_TARGET)/include/rpc
	@cp -f $(LIBRPC_SRCDIR)/rpc/*.h $(PTXCONF_SYSROOT_TARGET)/include/rpc
	@$(call touch, $@)

# ----------------------------------------------------------------------------
# Target-Install
# ----------------------------------------------------------------------------

$(STATEDIR)/librpc.targetinstall:
	@$(call targetinfo, $@)

	@$(call install_init, librpc)
	@$(call install_fixup, librpc,PACKAGE,librpc)
	@$(call install_fixup, librpc,PRIORITY,optional)
	@$(call install_fixup, librpc,VERSION,$(LIBRPC_VERSION))
	@$(call install_fixup, librpc,SECTION,base)
	@$(call install_fixup, librpc,DEPENDS,)
	@$(call install_fixup, librpc,DESCRIPTION,missing)

	@$(call install_copy, librpc, 0, 0, 0644, $(LIBRPC_DIR)/liblibrpc.so.$(LIBRPC_LIBVER), /opt/qcom/lib/liblibrpc.so.$(LIBRPC_LIBVER))
	@$(call install_link, librpc, liblibrpc.so.$(LIBRPC_LIBVER), /opt/qcom/lib/liblibrpc.so.$(LIBRPC_LIBMAJOR))
	@$(call install_link, librpc, liblibrpc.so.$(LIBRPC_LIBMAJOR), /opt/qcom/lib/liblibrpc.so)



	@$(call install_finish, librpc)
	@$(call touch, $@)

# ----------------------------------------------------------------------------
# Clean
# ----------------------------------------------------------------------------

librpc_clean:
	rm -rf $(STATEDIR)/librpc.*
	rm -rf $(PKGDIR)/librpc_*
	rm -rf $(LIBRPC_DIR)

# vim: syntax=make
